from enum import IntEnum, auto
import json

from pyarrow import flight


class RequestType(IntEnum):
    Iterate = auto()
    FindChannels = auto()
    CountChannels = auto()


def create_command(request_type, **kwargs):
    cmd = {
        "request": request_type.name,
        "args": kwargs,
    }
    return json.dumps(cmd).encode("utf-8")


def create_descriptor(request_type, **kwargs):
    cmd = create_command(request_type, **kwargs)
    return flight.FlightDescriptor.for_command(cmd)


def parse_command(cmd):
    parsed = json.loads(cmd.decode("utf-8"))
    return RequestType[parsed["request"]], parsed["args"]
