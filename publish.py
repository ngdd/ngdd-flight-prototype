import numpy

import arrakis
from arrakis import Time, DataBlock


series = {
    "H1:FKE-TEST_CHANNEL1": numpy.array([0.1, 0.2, 0.3, 0.4], dtype=numpy.float64),
    "H1:FKE-TEST_CHANNEL2": numpy.array([1.1, 1.2, 1.3, 1.4], dtype=numpy.float64),
}
block = DataBlock(1234567890 * Time.SECONDS, series)
print(block["H1:FKE-TEST_CHANNEL1"])

with arrakis.connect("kafka://localhost:9092") as client:
    client.publish(block)
