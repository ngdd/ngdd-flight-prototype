import pathlib
import re
import time

import numpy as np
import pyarrow as pa
from pyarrow import flight

import gpstime
from gwpy.detector import ChannelList

from utils import RequestType, create_command, parse_command


class FlightServer(flight.FlightServerBase):
    def __init__(
        self,
        location="grpc://0.0.0.0:31206",
        channel_list=pathlib.Path("channels.ini"),
        **kwargs
    ):
        super().__init__(location, **kwargs)
        self._location = location
        self._channels = ChannelList.read(channel_list)
        self._channel_map = {channel.name: channel for channel in self._channels}
        self._current = None

    def _make_iterate_schema(self, channels):
        columns = [pa.field("time", pa.int64())]
        for channel in channels:
            columns.append(pa.field(channel, pa.list_(pa.float64())))
        return pa.schema(columns)

    def _make_find_channels_schema(self):
        return pa.schema(
            [
                pa.field("channel", pa.string()),
                pa.field("sample_rate", pa.int32()),
                pa.field("data_type", pa.string()),
            ]
        )

    def _make_count_channels_schema(self):
        return pa.schema([pa.field("count", pa.int64())])

    def _make_flight_info(self, cmd):
        request, args = parse_command(cmd)
        if request is RequestType.Iterate:
            schema = self._make_iterate_schema(args["channels"])
        elif request is RequestType.FindChannels:
            schema = self._make_find_channels_schema()
        elif request is RequestType.CountChannels:
            schema = self._make_count_channels_schema()
        else:
            raise flight.FlightUnavailableError("command not understood")

        descriptor = flight.FlightDescriptor.for_path(cmd)
        endpoints = [flight.FlightEndpoint(cmd, [self._location])]
        return flight.FlightInfo(schema, descriptor, endpoints, -1, -1)

    def _find_channels(
        self, channel="*", data_type=None, channel_type=None, min_rate=None, max_rate=None
    ):
        if min_rate is None:
            min_rate = 0
        if max_rate is None:
            max_rate = 65536

        pattern = re.compile(channel)
        channels = []
        for channel in self._channel_map.values():
            if pattern.match(channel.name):
                rate = int(self._channel_map[channel.name].sample_rate.value)
                if rate >= min_rate and rate <= max_rate:
                    channels.append(channel)
        return channels

    def _generate_channel_data(self, schema, channels, start=None, end=None):
        # determine if 'live' or bounded stream
        is_live = not start and not end
        if is_live:
            current = (int(gpstime.gpsnow() * 1e6) // 62500) * 62500
        else:
            current = int(start * 1e6)

        # generate data
        while is_live or (current < int(end * 1e6)):
            channel_data = []
            for channel in channels:
                rate = int(self._channel_map[channel].sample_rate.value) // 16
                channel_data.append(
                    pa.array(
                        [np.array(np.random.normal(size=rate), dtype=np.float64)],
                        type=schema.field(channel).type,
                    )
                )

            yield pa.RecordBatch.from_arrays(
                [
                    pa.array(
                        [current],
                        type=schema.field("time").type,
                    ),
                    *channel_data,
                ],
                schema=schema,
            )

            # update timestamp
            current += 62500  # 1/16th of a second in microseconds

            # sleep if generating live data
            if is_live:
                time.sleep(max((current - int(gpstime.gpsnow() * 1e6)) / 1e6, 0))

    def list_flights(self, context, criteria):
        for channel in self._channels:
            yield self._make_flight_info(
                create_command(RequestType.Iterate, channels=[channel.name])
            )

    def get_flight_info(self, context, descriptor):
        return self._make_flight_info(descriptor.command)

    def do_get(self, context, ticket):
        request, kwargs = parse_command(ticket.ticket)
        if request is RequestType.Iterate:
            # parse iterate args
            channels = kwargs["channels"]
            start = kwargs["start"]
            end = kwargs["end"]

            # Generate data from requested channels
            schema = self._make_iterate_schema(channels)
            return flight.GeneratorStream(
                schema, self._generate_channel_data(schema, channels, start, end)
            )
        elif request is RequestType.FindChannels:
            channels = self._find_channels(**kwargs)
            schema = self._make_find_channels_schema()
            batch = pa.RecordBatch.from_arrays(
                [
                    pa.array(
                        [channel.name for channel in channels],
                        type=schema.field("channel").type,
                    ),
                    pa.array(
                        [int(channel.sample_rate.value) for channel in channels],
                        type=schema.field("sample_rate").type,
                    ),
                    pa.array(
                        ['float32' for _ in channels],
                        type=schema.field("data_type").type,
                    ),
                ],
                schema=schema,
            )
            return flight.RecordBatchStream(
                pa.RecordBatchReader.from_batches(schema, [batch])
            )
        elif request is RequestType.CountChannels:
            count = len(self._find_channels(**kwargs))
            schema = self._make_count_channels_schema()
            batch = pa.RecordBatch.from_arrays(
                [
                    pa.array(
                        [count],
                        type=schema.field("count").type,
                    ),
                ],
                schema=schema,
            )
            return flight.RecordBatchStream(
                pa.RecordBatchReader.from_batches(schema, [batch])
            )
        else:
            raise flight.FlightUnavailableError("command not understood")


if __name__ == "__main__":
    server = FlightServer()
    server.serve()
