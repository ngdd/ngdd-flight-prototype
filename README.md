# NGDD Arrow Flight Prototype

Here we have an Arrow Flight client and server implementation, serving
generated channel data for a subset of the set of channels for H1.

## Tutorial

1. Install Python libraries:

```
pip install -r requirements.txt
```

2. Run server

```
python server.py
```

3. Run client

```
python client.py
```
