from arrakis import fetch, find_channels, count_channels


# live or bounded data
#is_live = True
is_live = False

# set start/end appropriately
if is_live:
    start = end = None
else:
    start = 1234500000
    end = 1234500100

# channels to request
channels = [
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    "H1:LSC-POP_A_RF45_I_ERR_DQ",
]

## iterate through blocks starting at 'now'
for block in fetch(channels, start, end):
    #print(block.time, block)
    continue

# find/count channels matching this pattern
channel_glob = "H1:LSC-*"

# find channels
print(find_channels(channel_glob))

# count channels
print(count_channels(channel_glob))
